# Mail de la prof joanna.akrouche@hds.utc.fr
from math import exp

from fiabilipy import Component, System, Voter

# Definition des composants

Alim1 = Component('Alim1', 2.5e-4)
Alim2 = Component('Alim2', 2.5e-4)
P = Component('P', 2.28e-4)
M1 = Component('M1', 2.94e-4)
M2 = Component('M2', 2.94e-4)
Bus = Component('Bus', 1e-4)

procs = Voter(P, 2, 3)

# Definition du systeme S

S = System()

S['E'] = [Alim1, Alim2]

S[Alim1] = S[Alim2] = [Bus]

S[Bus] = [M1, M2]

S[M1] = S[M2] = [procs]

S[procs] = 'S'

Memoires = System()
Memoires['E'] = [M1, M2]
Memoires[M1] = Memoires[M2] = 'S'

Alims = System()
Alims['E'] = [Alim1, Alim2]
Alims[Alim1] = Alims[Alim2] = 'S'

# Calculs interessants

nb_heures = 100

## fiabilite des Memoires
rMemoires = Memoires.reliability(nb_heures)
print "fiabilite memoires", rMemoires

## fiabilite du voteur
rVoteur = procs.reliability(nb_heures)
print "fiabilite voteur", rVoteur

rAlim = Alim1.reliability(nb_heures)
r2Alim = Alims.reliability(nb_heures)
print "fiabilite alim", rAlim

# facteur d'importance du bus
psibusfonctionne = rMemoires * rVoteur * r2Alim
psibusfonctionnepas = 0

FIbus = psibusfonctionne - psibusfonctionnepas
print "FI bus", FIbus

# fiabilite du bus
rBus = Bus.reliability(nb_heures)

pvoteursivoteurfonctionne = 1 - (1 - P.reliability(nb_heures) ) **2
pvoteursivoteurfonctionnepas = P.reliability(nb_heures) **2

psivoteurfonctionne = rBus * r2Alim * rMemoires * pvoteursivoteurfonctionne
psivoteurfonctionnepas = rBus * r2Alim * rMemoires * pvoteursivoteurfonctionnepas

# facteur d'importance d'un proco
FIvoteur = psivoteurfonctionne - psivoteurfonctionnepas
print "FI processeur", FIvoteur

psimemoire = rBus * rVoteur * r2Alim
psimemoirepas = rBus * rVoteur * M2.reliability(nb_heures) * r2Alim
FImemoire = psimemoire - psimemoirepas
print "FI memoire", FImemoire


psialim = rBus * rVoteur * rMemoires 
psialimpas = rBus * rVoteur * rMemoires * rAlim
FIalim = psialim - psialimpas
print "FI alim", FIalim

