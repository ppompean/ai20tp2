<!-- mailto joanna.akrouche@hds.utc.fr -->

#### Paco Pompeani et Maylis de Talhouet
# Compte rendu TP2 - Blocs digrammes de Fiabilité

## Objectifs

Le but de ce TP était d'évaluer des paramètres de sûreté de fonctionnement de différentes architectures. Pour cela nous avons utilisé fiabilipy, une librairie python développée par des UTCéens.

Durant le TP, nous avons modélisé deux architectures afin de les comparer et proposer une troisième architecture.

### Architecture 0

#### Arbre de défaillance de l'architecture proposée

![Arbre de défaillance](https://u.ppom.me/archi0.svg =x500)

#### Programme permettant la modélisation avec Fiabilipy

```python
from math import exp
from fiabilipy import Component, System, Voter

# Definition des composants

P = Component('P', 2.28e-4)
M1 = Component('M1', 2.94e-4)
M2 = Component('M2', 2.94e-4)
Bus = Component('Bus', 1e-4)

procs = Voter(P, 2, 3)

# Definition du systeme S

S = System()

S['E'] = [Bus]

S[Bus] = [M1, M2]

S[M1] = S[M2] = [procs]

S[procs] = 'S'

Memoires = System()
Memoires['E'] = [M1, M2]
Memoires[M1] = Memoires[M2] = 'S'

# Calculs interessants

nb_heures = 100

## fiabilite des Memoires
rMemoires = Memoires.reliability(nb_heures)
print "fiabilite memoires", rMemoires

## fiabilite du voteur
rVoteur = procs.reliability(nb_heures)
print "fiabilite voteur", rVoteur

# facteur d'importance du bus
psibusfonctionne = rMemoires * rVoteur
psibusfonctionnepas = 0

FIbus = psibusfonctionne - psibusfonctionnepas
print "FI bus", FIbus

# fiabilite du bus
rBus = Bus.reliability(nb_heures)

pvoteursivoteurfonctionne = 1 - (1 - P.reliability(nb_heures) ) **2
pvoteursivoteurfonctionnepas = P.reliability(nb_heures) **2

psivoteurfonctionne = Bus.reliability(nb_heures) * rMemoires * pvoteursivoteurfonctionne
psivoteurfonctionnepas = Bus.reliability(nb_heures) * rMemoires * pvoteursivoteurfonctionnepas

# facteur d'importance d'un proco
FIvoteur = psivoteurfonctionne - psivoteurfonctionnepas
print "FI processeur", FIvoteur

psimemoire = rBus * rVoteur
psimemoirepas = rBus * rVoteur * M2.reliability(nb_heures)
FImemoire = psimemoire - psimemoirepas
print "FI memoire", FImemoire

```

#### Résultats obtenus

Voilà les résultats obtenus à l'exécution du programme ci-dessus :

Facteur d'importance du bus : 0.997660360076212
Facteur d'importance d'un processeur : 0.0435926974109617
Facteur d'importance d'une memoire : 0.0286406786760562

Ici, le bus est le *Single Point of Failure* du système.

#### Proposition d'amélioration de l'architecture 0

Nous ne savons pas si c'est réaliste, mais nous aimerions proposer de paralléliser

<!--
### Facteurs d'importance avec t = 1 ans 

1) Formule du facteur d'importance : $F_{imp} = \mathbb{P}(S|e) - \mathbb{P}(S|¬{e})$

"Probabilité que le système fonctionne si $e$ fonctionne moins la probabilité que le système fonctionne si $e$ ne fonctionne pas".

${ M1 / M2 }$, ${ P1 / P2 / P3 }$ et ${ A1 / A2 / A3 }$ ont le même $F_{imp}$.

Pour le calcul, nous allons **diviser le système en sous-systèmes**. 
- **SS1**: M1 et M2 (système en parallèle)
- **SS2**: Le voteur: il est composé de 3 processeurs en parallèle. C'est un voteur 2/3
- et le sytème total : composé de SS1, SS2 et du bus

La fiabilité d'un système en série est : $R(t) =\Pi_{i=1}^{n} \exp^{-\lambda_{i}t}$
La fiabilité d'un système en parallèle est : $1-(\Pi_{i=1}^{n} [1 - R(i)])$

**Calcul du facteur d'importance du Bus**

Si le Bus fonctionne, la fiabilité du système devient $R(SS1) * R(SS2)$.

$$R(SS1) = 1 exp(-2.94e-4*t)$$
-->

### Architecture 1

#### Arbre de défaillance de l'architecture proposée

![Arbre de défaillance](https://u.ppom.me/archi1.svg =x500)

#### Programme permettant la modélisation avec Fiabilipy

```python
# Mail de la prof joanna.akrouche@hds.utc.fr
from math import exp

from fiabilipy import Component, System, Voter

# Definition des composants

Alim = Component('Alim', 2.5e-4)
P = Component('P', 2.28e-4)
M1 = Component('M1', 2.94e-4)
M2 = Component('M2', 2.94e-4)
Bus = Component('Bus', 1e-4)

procs = Voter(P, 2, 3)

# Definition du systeme S

S = System()

S['E'] = [Alim]

S[Alim] = [Bus]

S[Bus] = [M1, M2]

S[M1] = S[M2] = [procs]

S[procs] = 'S'

Memoires = System()
Memoires['E'] = [M1, M2]
Memoires[M1] = Memoires[M2] = 'S'

# Calculs interessants

nb_heures = 100

## fiabilite des Memoires
rMemoires = Memoires.reliability(nb_heures)
print "fiabilite memoires", rMemoires

## fiabilite du voteur
rVoteur = procs.reliability(nb_heures)
print "fiabilite voteur", rVoteur

rAlim = Alim.reliability(nb_heures)
print "fiabilite alim", rAlim

# facteur d'importance du bus
psibusfonctionne = rMemoires * rVoteur * rAlim
psibusfonctionnepas = 0

FIbus = psibusfonctionne - psibusfonctionnepas
print "FI bus", FIbus

# fiabilite du bus
rBus = Bus.reliability(nb_heures)

pvoteursivoteurfonctionne = 1 - (1 - P.reliability(nb_heures) ) **2
pvoteursivoteurfonctionnepas = P.reliability(nb_heures) **2

psivoteurfonctionne = rBus * rAlim * rMemoires * pvoteursivoteurfonctionne
psivoteurfonctionnepas = rBus * rAlim * rMemoires * pvoteursivoteurfonctionnepas

# facteur d'importance d'un proco
FIvoteur = psivoteurfonctionne - psivoteurfonctionnepas
print "FI processeur", FIvoteur

psimemoire = rBus * rVoteur * rAlim
psimemoirepas = rBus * rVoteur * M2.reliability(nb_heures) * rAlim
FImemoire = psimemoire - psimemoirepas
print "FI memoire", FImemoire


psialim = rBus * rVoteur * rMemoires
psialimpas = 0
FIalim = psialim - psialimpas
print "FI alim", FIalim

```

#### Résultats obtenus

Voilà les résultats obtenus à l'exécution du programme ci-dessus :

Facteur d'importance du bus : 0.973028038020085
Facteur d'importance d'un processeur : 0.0425163898769627
Facteur d'importance d'une memoire : 0.0279335377999761
Facteur d'importance d'une alim : 0.987733473631589

Ici, on trouve deux *Single Points of Failure*, le bus et l'alimentation.

<!--
### Facteurs d'importance avec t = 1 ans 

1) Formule du facteur d'importance : $F_{imp} = \mathbb{P}(S|e) - \mathbb{P}(S|¬{e})$

"Probabilité que le système fonctionne si $e$ fonctionne moins la probabilité que le système fonctionne si $e$ ne fonctionne pas".

${ M1 / M2 }$, ${ P1 / P2 / P3 }$ et ${ A1 / A2 / A3 }$ ont le même $F_{imp}$.

Pour le calcul, nous allons **diviser le système en sous-systèmes**. 
- **SS1**: M1 et M2 (système en parallèle)
- **SS2**: Le voteur: il est composé de 3 processeurs en parallèle. C'est un voteur 2/3
- et le sytème total : composé de SS1, SS2 et du bus

La fiabilité d'un système en série est : $R(t) =\Pi_{i=1}^{n} \exp^{-\lambda_{i}t}$
La fiabilité d'un système en parallèle est : $1-(\Pi_{i=1}^{n} [1 - R(i)])$

**Calcul du facteur d'importance du Bus**

Si le Bus fonctionne, la fiabilité du système devient $R(SS1) * R(SS2)$.

$$R(SS1) = 1 exp(-2.94e-4*t)$$
-->

### Architecture 2

#### Arbre de défaillance de l'architecture proposée

![Arbre de défaillance](https://u.ppom.me/archi2.svg =x500)

#### Programme permettant la modélisation avec Fiabilipy

```python
# Mail de la prof joanna.akrouche@hds.utc.fr
from math import exp

from fiabilipy import Component, System, Voter

# Definition des composants

Alim1 = Component('Alim1', 2.5e-4)
Alim2 = Component('Alim2', 2.5e-4)
P = Component('P', 2.28e-4)
M1 = Component('M1', 2.94e-4)
M2 = Component('M2', 2.94e-4)
Bus = Component('Bus', 1e-4)

procs = Voter(P, 2, 3)

# Definition du systeme S

S = System()

S['E'] = [Alim1, Alim2]

S[Alim1] = S[Alim2] = [Bus]

S[Bus] = [M1, M2]

S[M1] = S[M2] = [procs]

S[procs] = 'S'

Memoires = System()
Memoires['E'] = [M1, M2]
Memoires[M1] = Memoires[M2] = 'S'

Alims = System()
Alims['E'] = [Alim1, Alim2]
Alims[Alim1] = Alims[Alim2] = 'S'

# Calculs interessants

nb_heures = 100

## fiabilite des Memoires
rMemoires = Memoires.reliability(nb_heures)
print "fiabilite memoires", rMemoires

## fiabilite du voteur
rVoteur = procs.reliability(nb_heures)
print "fiabilite voteur", rVoteur

rAlim = Alim1.reliability(nb_heures)
r2Alim = Alims.reliability(nb_heures)
print "fiabilite alim", rAlim

# facteur d'importance du bus
psibusfonctionne = rMemoires * rVoteur * r2Alim
psibusfonctionnepas = 0

FIbus = psibusfonctionne - psibusfonctionnepas
print "FI bus", FIbus

# fiabilite du bus
rBus = Bus.reliability(nb_heures)

pvoteursivoteurfonctionne = 1 - (1 - P.reliability(nb_heures) ) **2
pvoteursivoteurfonctionnepas = P.reliability(nb_heures) **2

psivoteurfonctionne = rBus * r2Alim * rMemoires * pvoteursivoteurfonctionne
psivoteurfonctionnepas = rBus * r2Alim * rMemoires * pvoteursivoteurfonctionnepas

# facteur d'importance d'un proco
FIvoteur = psivoteurfonctionne - psivoteurfonctionnepas
print "FI processeur", FIvoteur

psimemoire = rBus * rVoteur * r2Alim
psimemoirepas = rBus * rVoteur * M2.reliability(nb_heures) * r2Alim
FImemoire = psimemoire - psimemoirepas
print "FI memoire", FImemoire


psialim = rBus * rVoteur * rMemoires 
psialimpas = rBus * rVoteur * rMemoires * rAlim
FIalim = psialim - psialimpas
print "FI alim", FIalim

```

#### Résultats obtenus

Voilà les résultats obtenus à l'exécution du programme ci-dessus :

<manquant, le fichier est vide>


<!--
### Facteurs d'importance avec t = 1 ans 

1) Formule du facteur d'importance : $F_{imp} = \mathbb{P}(S|e) - \mathbb{P}(S|¬{e})$

"Probabilité que le système fonctionne si $e$ fonctionne moins la probabilité que le système fonctionne si $e$ ne fonctionne pas".

${ M1 / M2 }$, ${ P1 / P2 / P3 }$ et ${ A1 / A2 / A3 }$ ont le même $F_{imp}$.

Pour le calcul, nous allons **diviser le système en sous-systèmes**. 
- **SS1**: M1 et M2 (système en parallèle)
- **SS2**: Le voteur: il est composé de 3 processeurs en parallèle. C'est un voteur 2/3
- et le sytème total : composé de SS1, SS2 et du bus

La fiabilité d'un système en série est : $R(t) =\Pi_{i=1}^{n} \exp^{-\lambda_{i}t}$
La fiabilité d'un système en parallèle est : $1-(\Pi_{i=1}^{n} [1 - R(i)])$

**Calcul du facteur d'importance du Bus**

Si le Bus fonctionne, la fiabilité du système devient $R(SS1) * R(SS2)$.

$$R(SS1) = 1 exp(-2.94e-4*t)$$
-->

### Architecture 3

#### Arbre de défaillance de l'architecture proposée

![Arbre de défaillance](https://u.ppom.me/archi3.svg =x500)

#### Programme permettant la modélisation avec Fiabilipy

```python
# Mail de la prof joanna.akrouche@hds.utc.fr
from math import exp

from fiabilipy import Component, System, Voter

# Definition des composants

Alim = Component('Alim', 2.5e-4)
P = Component('P', 2.28e-4)
M1 = Component('M1', 2.94e-4)
M2 = Component('M2', 2.94e-4)
Bus = Component('Bus', 1e-4)

procs = Voter(P, 2, 3)

votalims = Voter(Alim, 2, 3)

# Definition du systeme S

S = System()

S['E'] = [votalims]

S[votalims] = [Bus]

S[Bus] = [M1, M2]

S[M1] = S[M2] = [procs]

S[procs] = 'S'

Memoires = System()
Memoires['E'] = [M1, M2]
Memoires[M1] = Memoires[M2] = 'S'

# Calculs interessants

nb_heures = 100

## fiabilite des Memoires
rMemoires = Memoires.reliability(nb_heures)
print "fiabilite memoires", rMemoires

## fiabilite du voteur
rVoteur = procs.reliability(nb_heures)
print "fiabilite voteur", rVoteur

rAlim = Alim.reliability(nb_heures)
r3Alim = votalims.reliability(nb_heures)
print "fiabilite alim", rAlim

# facteur d'importance du bus
psibusfonctionne = rMemoires * rVoteur * r3Alim
psibusfonctionnepas = 0

FIbus = psibusfonctionne - psibusfonctionnepas
print "FI bus", FIbus

# fiabilite du bus
rBus = Bus.reliability(nb_heures)

pvoteursivoteurfonctionne = 1 - (1 - P.reliability(nb_heures) ) **2
pvoteursivoteurfonctionnepas = P.reliability(nb_heures) **2

psivoteurfonctionne = rBus * r3Alim * rMemoires * pvoteursivoteurfonctionne
psivoteurfonctionnepas = rBus * r3Alim * rMemoires * pvoteursivoteurfonctionnepas

# facteur d'importance d'un proco
FIvoteur = psivoteurfonctionne - psivoteurfonctionnepas
print "FI processeur", FIvoteur

psimemoire = rBus * rVoteur * r3Alim
psimemoirepas = rBus * rVoteur * M2.reliability(nb_heures) * r3Alim
FImemoire = psimemoire - psimemoirepas
print "FI memoire", FImemoire

psialim = rBus * rMemoires * rVoteur * (1 - (1- Alim.reliability(nb_heures)) **2)
psialimpas = rBus * rMemoires * rVoteur * (Alim.reliability(nb_heures) **2)

FIalim = psialim - psialimpas
print "FI alim", FIalim
```

#### Résultats obtenus

Voilà les résultats obtenus à l'exécution du programme ci-dessus :

Facteur d'importance du bus : 0.995865869229602
Facteur d'importance d'un processeur : 0.0435142872629660
Facteur d'importance d'une memoire : 0.0285891627115240
Facteur d'importance d'une alim : 0.0475702071847940

Le bus est à nouveau le seul maillon faible du système.

<!--
### Facteurs d'importance avec t = 1 ans 

1) Formule du facteur d'importance : $F_{imp} = \mathbb{P}(S|e) - \mathbb{P}(S|¬{e})$

"Probabilité que le système fonctionne si $e$ fonctionne moins la probabilité que le système fonctionne si $e$ ne fonctionne pas".

${ M1 / M2 }$, ${ P1 / P2 / P3 }$ et ${ A1 / A2 / A3 }$ ont le même $F_{imp}$.

Pour le calcul, nous allons **diviser le système en sous-systèmes**. 
- **SS1**: M1 et M2 (système en parallèle)
- **SS2**: Le voteur: il est composé de 3 processeurs en parallèle. C'est un voteur 2/3
- et le sytème total : composé de SS1, SS2 et du bus

La fiabilité d'un système en série est : $R(t) =\Pi_{i=1}^{n} \exp^{-\lambda_{i}t}$
La fiabilité d'un système en parallèle est : $1-(\Pi_{i=1}^{n} [1 - R(i)])$

**Calcul du facteur d'importance du Bus**

Si le Bus fonctionne, la fiabilité du système devient $R(SS1) * R(SS2)$.

$$R(SS1) = 1 exp(-2.94e-4*t)$$
-->
